package main

type Employee struct {
	Eid       int     `json."eid"`
	EmpName   string  `json:"empname"`
	EmpSalary float64 `json:"salary"`
	Emaail    string  `json:"email"`
}
