module gitlab.com/bombk/goweb

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	gorm.io/driver/mysql v1.3.2 // indirect
	gorm.io/gorm v1.23.3 // indirect
)
