package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func HandlerRoutine() {
	r := mux.NewRouter()
	r.HandleFunc("/employee", CreateEmployee).Methods("POST")
	r.HandleFunc("/employees", GetEmployees).Methods("GET")
	r.HandleFunc("/employee/{eid}", GetEmployeesByID).Methods("GET")
	r.HandleFunc("/update/{eid}", UpdateEmployee).Methods("PUT")
	r.HandleFunc("/delete/{eid}", DeleteEmployee).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8088", r))
}
