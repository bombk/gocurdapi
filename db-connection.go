package main

import (
	"gorm.io/driver/mysql"

	"fmt"

	"gorm.io/gorm"
)

var Database *gorm.DB

var urlDSN = "root:@tcp(localhost:3306)/mydb?parseTime=true"

//var urlDSN = "username:password@tcp(localhost:3306)/mydb"

var err error

func DataMigration() {
	Database, err = gorm.Open(mysql.Open(urlDSN), &gorm.Config{})
	if err != nil {
		fmt.Print(err.Error())
		panic("connection failed")
	}
	Database.AutoMigrate(&Employee{})

}
